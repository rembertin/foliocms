<?php
/**
 * Created by PhpStorm.
 * User: rembertin
 * Date: 19/04/16
 * Time: 20:06
 */
return [
    /*
    |--------------------------------------------------------------------------
    | Base route
    |--------------------------------------------------------------------------
    |
    | You can configure the base route of the back-office. For example if the value
    | is 'admin', the back-office will be accessible from 'yourdomain.com/admin'
    |
    */
    'route' => 'admin'
];