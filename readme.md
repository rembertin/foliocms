# FolioCMS

FolioCMS is a open-source and modular CMS that was based on Laravel framework.

It has a very clean and modern administration panel, which follow the rules of Google's Material Design.

![FolioCMS ScreenShot](http://img15.hostingpics.net/pics/971970Capturedu20170112223634.png)