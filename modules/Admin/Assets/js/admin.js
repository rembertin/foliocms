'use strict';

var $body = $(document.body);
$("#sidenav-toggle").click(function () {
    $body.toggleClass('show-sidenav');
});
// Initialize collapse button
$(".button-collapse").sideNav();
// Initialize collapsible (uncomment the line below if you use the dropdown variation)
$('.collapsible').collapsible();