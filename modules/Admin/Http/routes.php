<?php

Route::group(['middleware' => 'web', 'prefix' => 'admin', 'namespace' => 'Modules\Admin\Http\Controllers'], function()
{
    Route::group(['middleware' => 'auth.admin:admin'], function ()
    {
        Route::get('pages', 'PagesController@index')->name('admin.pages.index');
        Route::get('pages/create', 'PagesController@create')->name('admin.pages.create');
        Route::post('pages', 'PagesController@store')->name('admin.pages.store');
        Route::get('pages/{id}', 'PagesController@edit')->name('admin.pages.edit');
        Route::post('pages/{id}', 'PagesController@update')->name('admin.pages.update');
        Route::post('pages/{id}/delete', 'PagesController@destroy')->name('admin.pages.destroy');
        Route::post('pages/{id}/activate', 'PagesController@activate')->name('admin.pages.activate');
        Route::get('/', 'AdminController@index')->name('admin.index');
    });

    Route::group(['namespace' => 'Auth'], function() {
        // Authentication Routes...
        Route::get('login', 'LoginController@showLoginForm')->name('admin.loginForm');
        Route::post('login', 'LoginController@login')->name('admin.login');
        Route::post('logout', 'LoginController@logout')->name('admin.logout');

        // Registration Routes...
        Route::get('register', 'RegisterController@showRegistrationForm')->name('admin.registrationForm');
        Route::post('register', 'RegisterController@register')->name('admin.register');

        // Password Reset Routes...
        Route::get('password/reset/{token?}', 'ResetPasswordController@showResetForm')->name('admin.resetForm');
        Route::post('password/email', 'ResetPasswordController@sendResetLinkEmail')->name('admin.resetLinkEmail');
        Route::post('password/reset', 'ResetPasswordController@reset')->name('admin.resetForm')->name('reset');
    });
});