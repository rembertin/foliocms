<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller;

class AdminController extends Controller
{
    protected $viewPrefix = 'admin::';

    protected function view($view = null, $data = [], $mergeData = [])
    {
        return view($view !== null ? $this->viewPrefix . $view : null, $data, $mergeData);
    }

    public function index()
    {
        debug(Auth::guard('admin')->user());
        return $this->view('index');
    }
}
