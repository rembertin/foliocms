@extends('admin::layouts.auth')

@section('main')
    <div class="container">
        <div class="row">
            <div class="col m8 offset-m2">
                <div class="card">
                    <div class="card-content">
                        <div class="card-title center-align">Connectez-vous pour accéder à l'interface d'administration</div>
                        {!! Form::open(['route' => 'admin.login']) !!}
                        @if (count($errors) > 0)
                            <div class="bg-error card-panel">
                                @foreach ($errors->all() as $error)
                                    <div class="white-text">{{ $error }}</div>
                                @endforeach
                            </div>
                        @endif

                        {!! Form::materialInput('email','email', 'Adresse email', null, ['id' => 'email']) !!}
                        {!! Form::materialInput('password','password', 'Mot de passe', null, ['id' => 'password']) !!}
                        {!! Form::materialInput('checkbox','remember', 'Se souvenir de moi', null, ['id' => 'remember']) !!}
                        <div class="input-field">
                            {!! Form::submit('Connexion', ['class' => 'btn waves-effect']) !!}
                        </div>
                        <a href="{{ route('admin.resetForm') }}">Mot de passe oublié ?</a>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
