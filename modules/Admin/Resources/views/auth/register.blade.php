@extends('admin::layouts.auth')

@section('main')
    <div class="container">
        <div class="row">
            <div class="col m8 offset-m2">
                <div class="card">
                    <div class="card-content">
                        <div class="card-title center-align">Inscription</div>
                        {!! Form::open(['route' => 'admin.register']) !!}
                        @if (count($errors) > 0)
                            <div class="bg-error card-panel">
                                @foreach ($errors->all() as $error)
                                    <div class="white-text">{{ $error }}</div>
                                @endforeach
                            </div>
                        @endif

                        {!! Form::materialText('last_name','Nom', null, ['id' => 'last_name']) !!}
                        {!! Form::materialText('first_name','Prénom', null, ['id' => 'first_name']) !!}
                        {!! Form::materialInput('email','email', 'Adresse email', null, ['id' => 'email']) !!}
                        {!! Form::materialInput('password','password', 'Mot de passe', null, ['id' => 'password']) !!}
                        {!! Form::materialInput('password','password_confirmation', 'Confirmation du mot de passe', null, ['id' => 'password_confirmation']) !!}
                        <div class="input-field">
                            {!! Form::submit('Inscription', ['class' => 'btn waves-effect']) !!}
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
