<div class="input-field">
    {!! Form::textarea($name, $value, $attributes) !!}
    {!! Form::label($name, $label) !!}
</div>