<div class="switch">
    <label>
        {{ $textBefore }}
        {!! Form::hidden($name, 0) !!}
        {!! Form::checkbox($name, 1, $checked, $options = []) !!}
        <span class="lever"></span>
        {{ $textAfter }}
    </label>
</div>