<div class="input-field">
    {!! Form::input($type, $name, $value, $attributes) !!}
    {!! Form::label($name, $label) !!}
</div>