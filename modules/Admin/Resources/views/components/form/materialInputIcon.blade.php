<div class="input-field">
    <i class="material-icons prefix">{{ $icon }}</i>
    {!! Form::input($type, $name, $value, $attributes) !!}
    {!! Form::label($name, $label) !!}
</div>