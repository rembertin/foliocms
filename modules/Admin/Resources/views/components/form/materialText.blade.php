<div class="input-field">
    {!! Form::text($name, $value, $attributes) !!}
    {!! Form::label($name, $label) !!}
</div>