@extends('admin::layouts.admin')

@section('main')
    <div class="container">
        <h2>Ajouter une page</h2>
        {!! Form::open(['route' => 'admin.pages.store']) !!}
        {!! Form::materialText('title','Titre de la page', null, ['id' => 'title']) !!}
        {!! Form::materialText('url','URL d\'accès', null, ['id' => 'url', 'placeholder' => "Saisissez l'URL"]) !!}
        {!! Form::materialSwitch('active', false, ['id' => 'active'], '', 'Page active') !!}

        <div class="input-field">
            {!! Form::submit('Créer', ['class' => 'btn waves-effect']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@push('scripts')
<script>
    $('#url').slugify('#title');
</script>
@endpush