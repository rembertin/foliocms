@extends('admin::layouts.admin')

@section('main')
    <ul class="collection with-header">
        <li class="collection-header"><h4>Gestion des pages</h4></li>
        @foreach($pages as $page)
            <li class="collection-item">
                @include('admin::pages.activateButton', compact('page'))
                <a href="{{ route('admin.pages.edit', ['id' => $page->id]) }}" class="title">{{ $page->title }}</a>
                <div class="secondary-content">
                    <a href="{{ route('admin.pages.edit', ['id' => $page->id]) }}"><i class="material-icons">mode_edit</i></a>
                    <a href="{{ route('admin.pages.destroy', ['id' => $page->id]) }}"><i class="material-icons red-text">delete</i></a>
                </div>
            </li>
        @endforeach
    </ul>
@endsection
@push('scripts')
<script>
    $('.page-activate').change(function () {
        var activate = this.checked ? 1 : 0;
        $.post($(this).attr('data-action'), {
            '_token': $('meta[name=csrf-token]').attr('content'),
            activate: activate
        });
    });
</script>
@endpush