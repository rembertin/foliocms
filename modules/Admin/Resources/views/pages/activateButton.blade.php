<span class="switch">
    <label>
        <input type="checkbox" class="page-activate" data-action="{{ route('admin.pages.activate', ['pages' => $page->id]) }}"
               @if($page->active) checked @endif>
        <span class="lever"></span>
    </label>
</span>