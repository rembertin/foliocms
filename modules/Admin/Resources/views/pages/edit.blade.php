@extends('admin::layouts.admin')

@section('main')
    <div class="container">
        <h2>Modifier une page</h2>
        {!! Form::open(['route' => ['admin.pages.update', $page->id]]) !!}
        {!! Form::materialText('title','Titre de la page', $page->title, ['id' => 'title']) !!}
        <div class="input-field">
            <i class="prefix">/</i>
            {!! Form::text('url', $page->url, ['id' => 'url', 'placeholder' => "Saisissez l'URL"]) !!}
            {!! Form::label('url', 'URL d\'accès') !!}
        </div>
        {!! Form::materialSwitch('active', $page->active, ['id' => 'active'], '', 'Page active') !!}

        <div class="input-field">
            {!! Form::submit('Sauvegarder', ['class' => 'btn waves-effect']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@push('scripts')
<script>
    var urlField = $('#url');
    if (! urlField.val()) {
        urlField.slugify('#title');
    }
</script>
@endpush