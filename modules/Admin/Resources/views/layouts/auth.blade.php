@extends('admin::layouts.master')

@section('content')
    <header>
        <nav>
            <div class="nav-wrapper">
                <a href="{{route('admin.index')}}" class="brand-logo waves-effect waves-light navbar-height">
                    <img src="{{asset('img/logo.png')}}" class="navbar-height" alt="Logo">
                </a>
                <ul id="nav-mobile" class="right">
                    <li><a href="{{ route('admin.loginForm') }}">Connexion</a></li>
                    <li><a href="{{ route('admin.registrationForm') }}">Inscription</a></li>
                </ul>
            </div>
        </nav>
    </header>
    <main>
        @yield('main')
    </main>
@endsection