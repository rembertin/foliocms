<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <meta name="csrf-token" content="{{ Session::token() }}">
    <link rel="stylesheet" href="{{ asset('css/dist.css') }}?1">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <title>@yield('title', 'Folio CMS')</title>
</head>
<body class="@yield('body.class')">
    @yield('content')
    <script type="application/javascript" src="{{ asset('js/dist.js') }}"></script>
    @stack('scripts')
</body>
</html>