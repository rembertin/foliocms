@extends('admin::layouts.master')

@section('body.class', 'with-sidenav')

@section('content')
    <header>
        <nav>
            <div class="nav-wrapper">
                <a href="#" data-activates="slide-out" class="button-collapse"><i
                            class="material-icons">menu</i></a>
                <a href="{{route('admin.index')}}" class="brand-logo center waves-effect waves-light navbar-height">
                    <img src="{{asset('img/logo.png')}}" class="navbar-height" alt="Logo">
                </a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a href="{{ route('admin.logout') }}">Se déconnecter</a></li>
                </ul>
            </div>
        </nav>
        <div id="slide-out" class="side-nav fixed">
            <a href="{{route('admin.index')}}" class="waves-effect waves-light company-logo">
                <img src="{{asset('img/logo_placeholder.png')}}" alt="Logo">
            </a>
            <ul class="collapsible" data-collapsible="expandable">
                <li>
                    <a class="collapsible-header waves-effect ">Pages<i
                                class="material-icons">arrow_drop_down</i></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="{{route('admin.pages.index')}}">Toutes les pages</a></li>
                            <li><a href="{{route('admin.pages.create')}}">Ajouter une page</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </header>
    <main>
        @yield('main')
    </main>

@endsection