var gulp = require('gulp')
var runSequence = require('run-sequence').use(gulp);
var concat = require('gulp-concat');
var concatCss = require('gulp-concat-css');
var cleanCss = require('gulp-clean-css');
var imageResize = require('gulp-image-resize');
var es = require('event-stream');
var sass = require('gulp-sass');

/** -- Define the stylesheets, js, fonts and media in variables -- **/
var styleFiles = [
    'bower_components/mdi/css/materialdesignicons.min.css',
    'modules/Admin/Assets/css/*',
    'modules/Admin/Assets/sass/*'
];

var fontFiles = [
    'bower_components/mdi/fonts/*',
    'bower_components/materialize/fonts/**/*'
];

var jsFiles = [
    'bower_components/jquery/dist/jquery.min.js',
    'bower_components/materialize/dist/js/materialize.min.js',
    'bower_components/speakingurl/speakingurl.min.js',
    'bower_components/jquery-slugify/dist/slugify.min.js',
    'modules/Admin/Assets/js/*'
];

var mediasFiles = [
    'modules/Admin/Assets/images/logo/favicon.ico'
];

/** -- Let's define some subtasks for CSS, Fonts and JS -- **/

gulp.task('style', function () {
    return gulp.src(styleFiles)
        .pipe(sass({includePaths:'./'}).on('error', sass.logError))
        .pipe(concatCss('dist.css'))
        .pipe(cleanCss())
        .pipe(gulp.dest('public/css/'));
});

gulp.task('style:watch', function () {
    gulp.watch(styleFiles, ['style']);
});

gulp.task('font', function () {
    return gulp.src(fontFiles)
        .pipe(gulp.dest('public/fonts'));
});
gulp.task('js', function () {
    return gulp.src(jsFiles)
        .pipe(concat('dist.js'))
        .pipe(gulp.dest('public/js'));
});
gulp.task('js:watch', function () {
    gulp.watch(jsFiles, ['js']);
});
gulp.task('media', function () {
    return es.concat(
        gulp.src(mediasFiles)
            .pipe(gulp.dest('public/medias')),
        gulp.src('modules/Admin/Assets/images/logo/logo.png')
            .pipe(imageResize({
                width: 250,
                background: 'none',
                imageMagick: true
            }))
            .pipe(gulp.dest('public/img'))
    );
});

/** --  Here is the place where all tasks are executed. -- **/

gulp.task('admin', function () {
    runSequence([
        'style',
        'font',
        'js',
        'media'
    ]);
});

gulp.task('admin:watch', function () {
    runSequence([
        'style:watch',
        'js:watch'
    ]);
});