<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Page
 *
 * @property integer $id
 * @property string $uuid
 * @property string $title
 * @property string $content
 * @property boolean $active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Modules\Admin\Entities\Page whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Admin\Entities\Page whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Admin\Entities\Page whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Admin\Entities\Page whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Admin\Entities\Page whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Admin\Entities\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Admin\Entities\Page whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Page extends Model
{
    protected $fillable = [
        'title', 'url', 'active'
    ];
}
